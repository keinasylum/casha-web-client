historyModule.controller('BalanceHistoryController', function ($modal, BalanceHistory, ngTableParams) {
    var self = this;

    BalanceHistory.getAll().then(function (result) {
        self.histories = result;
        self.balanceHistoryTable = new ngTableParams({
            page: 0
        }, {
            getData: function ($defer, params) {
                params.total(self.histories.length);
                $defer.resolve(self.histories.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                if (params.page() == 0) {
                    var elementsOnPage = 25;
                    params.count(elementsOnPage);
                    params.page(Math.ceil(self.histories.length / elementsOnPage));
                }
            }
        });        
    });
});
