historyModule.filter('action', function () {
    return function (input) {
        var result = "";
        if (input == "operationSave") {
            result = "Сохранение операции";
        } else if (input == "operationUpdate") {
            result = "Обновление операции";
        } else if (input == "operationDelete") {
            result = "Удаление операции";
        } else if (input == "transferSave") {
            result = "Сохранение перевода";
        } else if (input == "transferUpdate") {
            result = "Обновление перевода";
        } else if (input == "transferDelete") {
            result = "Удаление перевода";
        }
        return result;
    };
});