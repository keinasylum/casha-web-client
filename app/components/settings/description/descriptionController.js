settingsModule.controller('DescriptionController', function($scope, Merge, Operation, UtilService, $modal) {
    $scope.getPrefillingValidateClass = UtilService.getPrefillingValidateClass;
    $scope.getValidateClass = UtilService.getValidateClass;
    $scope.isEdit = UtilService.isEdit;

    $scope.descriptions = [];
    $scope.showMergeForm = false;
    $scope.errors = [];
    $scope.DescriptionMergeRequest = {
        descriptions: [],
        newDescriptionName: null
    };

    $scope.loadDescriptions = function () {
        Operation.getUniqueDescriptions().then(function (descriptions) {
            $scope.descriptions = descriptions;
        });
    };
    $scope.loadDescriptions();

    $scope.toggleShowMergeFormFlag = function (description) {
        UtilService.markItemMergeableFlag($scope.descriptions, description);
        var descriptionsObject = UtilService.getMergeableItems($scope.descriptions);
        if (descriptionsObject.length > 0) {
            $scope.DescriptionMergeRequest.descriptions = [];
            angular.forEach(descriptionsObject, function (item, key) {
                $scope.DescriptionMergeRequest.descriptions.push(item.name);
            });
            $scope.showMergeForm = true;
        } else {
            $scope.showMergeForm = false;
        }
    };

    $scope.cancelMerging = function () {
        $scope.DescriptionMergeRequest.descriptions = [];
        $scope.showMergeForm = false;
        $scope.loadDescriptions();
    };

    $scope.closeErrorAlert = function (index) {
        $scope.errors.splice(index, 1);
    };

    $scope.openMergeDescription = function (form) {
        var modalInstance = $modal.open({
            templateUrl: 'mergeDialog',
            controller: 'DialogController',
            size: 'sm'
        });
        modalInstance.result.then(function (confirmed) {
            if (confirmed && form && form.$valid) {
                Merge.mergeDescriptions($scope.DescriptionMergeRequest).then(function () {
                    $scope.showMergeForm = false;
                    $scope.loadDescriptions();
                    $scope.DescriptionMergeRequest = UtilService.resetFields($scope.DescriptionMergeRequest);
                    form.$setPristine();
                }, function (error) {
                    $scope.errors.push({message: UtilService.getExceptionMessage(error)});
                });
            }
        });
    };
});