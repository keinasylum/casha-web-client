settingsModule.controller('AccountController', function ($scope, Account, $modal, UtilService) {
	$scope.getPrefillingValidateClass = UtilService.getPrefillingValidateClass;
	$scope.getValidateClass = UtilService.getValidateClass;
	$scope.isEdit = UtilService.isEdit;

	$scope.showAccountForm = false;
	$scope.errors = [];
	$scope.AccountRequest = {
		name: null,
		balance: null
	};

	$scope.toggleShowAccountFormFlag = function () {
		if ($scope.showAccountForm) {
			$scope.showAccountForm = false;
		} else {
			$scope.showAccountForm = true;
		}
	};

	$scope.closeErrorAlert = function (index) {
		$scope.errors.splice(index, 1);
	};

	$scope.loadAccounts = function (forcibly) {
		if (!$scope.accounts || forcibly) {
			Account.getAll().then(function (result) {
				$scope.accounts = result;
			});
		}
	};
	$scope.loadAccounts(false);

	$scope.saveAccount = function (form) {
		if (form && form.$valid) {
			Account.save($scope.AccountRequest).then(function () {
				$scope.loadAccounts(true);
				$scope.showAccountForm = false;
				$scope.AccountRequest = UtilService.resetFields($scope.AccountRequest);
				form.$setPristine();
			}, function (error) {
				$scope.errors.push({message: UtilService.getExceptionMessage(error)});
			});
		}
	};

	$scope.prepareToEdit = function (editable) {
		UtilService.markItemEditableFlag($scope.accounts, editable, true);
		$scope.loadAccounts(false);
	};

	$scope.edit = function (editable, form) {
		UtilService.markItemEditableFlag($scope.accounts, editable, false);
		if (form && form.$valid) {
			Account.update(editable).then(function () {
				$scope.loadAccounts(true);
			}, function (error) {
				$scope.errors.push({message: UtilService.getExceptionMessage(error)});
			});
		}
	};

	$scope.openDeleteAccount = function (removable) {
		var modalInstance = $modal.open({
			templateUrl: 'deleteDialog',
			controller: 'DialogController',
			size: 'sm'
		});
		modalInstance.result.then(function (confirmed) {
			if (confirmed) {
				Account.remove(removable).then(function () {
					$scope.loadAccounts(true);
				}, function (error) {
					$scope.errors.push({message: UtilService.getExceptionMessage(error)});
				});
			}
		});
	};
});
