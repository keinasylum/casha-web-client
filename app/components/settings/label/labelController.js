settingsModule.controller('LabelController', function ($scope, Label, $modal, UtilService, Merge, Category) {
    $scope.getPrefillingValidateClass = UtilService.getPrefillingValidateClass;
    $scope.getValidateClass = UtilService.getValidateClass;
    $scope.isEdit = UtilService.isEdit;
    $scope.showLabelForm = false;
    $scope.showMergeForm = false;
    $scope.errors = [];
    $scope.operationTypes = {
        income: {
            name: 'Доход',
            requestName: 'income'
        },
        expense: {
            name: 'Расход',
            requestName: 'expense'
        }
    };
    $scope.LabelRequest = {
        name: null,
        operationType: null,
        categories: []
    };
    $scope.LabelMergeRequest = {
        mergedLabels: [],
        newLabelName: null
    };

    $scope.toggleShowLabelFormFlag = function () {
        $scope.showLabelForm = !$scope.showLabelForm;
        if ($scope.showLabelForm) {
            Category.getCategories().then(function (data) {
                $scope.categories = data;
            });
        }
    };

    $scope.toggleShowMergeFormFlag = function (label) {
        UtilService.markItemMergeableFlag($scope.labels, label);
        var labels = UtilService.getMergeableItems($scope.labels);
        if (labels.length > 0) {
            $scope.LabelMergeRequest.mergedLabels = labels;
            $scope.showMergeForm = true;
        } else {
            $scope.showMergeForm = false;
        }
    };

    $scope.cancelMerging = function () {
        $scope.LabelMergeRequest.mergedLabels = [];
        $scope.showMergeForm = false;
        $scope.loadLabels();
    };

    $scope.closeErrorAlert = function (index) {
        $scope.errors.splice(index, 1);
    };

    $scope.loadLabels = function () {
        Label.getLabels().then(function (result) {
            $scope.labels = result;
        });
    };
    $scope.loadLabels();

    $scope.saveLabel = function (form) {
        if (form && form.$valid) {
            var savableCategories = UtilService.getTickedItems($scope.categories);
            $scope.LabelRequest.categories = UtilService.cleanCategories(savableCategories);
            Label.save($scope.LabelRequest).then(function () {
                UtilService.setItemsUnticked($scope.categories);
                $scope.loadLabels();
                $scope.showLabelForm = false;
                $scope.LabelRequest = UtilService.resetFields($scope.LabelRequest);
                form.$setPristine();
            }, function (error) {
                $scope.errors.push({message: UtilService.getExceptionMessage(error)});
            });
        }
    };

    $scope.edit = function (editable, form) {
        UtilService.markItemEditableFlag($scope.labels, editable, false);
        if (form && form.$valid) {
            var updatableCategories = UtilService.getTickedItems($scope.categories);
            editable.categories = UtilService.cleanCategories(updatableCategories);
            Label.update(editable).then(function () {
                UtilService.setItemsUnticked($scope.categories);
                $scope.loadLabels();
            }, function (error) {
                $scope.errors.push({message: UtilService.getExceptionMessage(error)});
            });
        }
    };

    $scope.prepareToEdit = function (editable) {
        UtilService.markItemsAsTicked($scope.categories, editable.categories);
        UtilService.markItemEditableFlag($scope.labels, editable, true);
        $scope.loadLabels();
    };

    $scope.openDeleteLabel = function (removable) {
        var modalInstance = $modal.open({
            templateUrl: 'deleteDialog',
            controller: 'DialogController',
            size: 'sm'
        });
        modalInstance.result.then(function (confirmed) {
            if (confirmed) {
                Label.remove(removable).then(function () {
                    $scope.loadLabels();
                }, function (error) {
                    $scope.errors.push({message: UtilService.getExceptionMessage(error)});
                });
            }
        });
    };

    $scope.openMergeLabel = function (form) {
        var modalInstance = $modal.open({
            templateUrl: 'mergeDialog',
            controller: 'DialogController',
            size: 'sm'
        });
        modalInstance.result.then(function (confirmed) {
            if (confirmed && form && form.$valid) {
                Merge.mergeLabels($scope.LabelMergeRequest).then(function () {
                    $scope.showMergeForm = false;
                    $scope.loadLabels();
                    $scope.LabelMergeRequest = UtilService.resetFields($scope.LabelMergeRequest);
                    form.$setPristine();
                }, function (error) {
                    $scope.errors.push({message: UtilService.getExceptionMessage(error)});
                });
            }
        });
    };
});