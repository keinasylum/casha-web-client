settingsModule.controller('CategoryController', function ($scope, $modal, UtilService, Category) {
    $scope.getPrefillingValidateClass = UtilService.getPrefillingValidateClass;
    $scope.getValidateClass = UtilService.getValidateClass;
    $scope.isEdit = UtilService.isEdit;
    $scope.categories = [];
    $scope.showCategoryForm = false;
    $scope.showMergeForm = false;
    $scope.errors = [];
    $scope.operationTypes = {
        income: {
            name: 'Доход',
            requestName: 'income'
        },
        expense: {
            name: 'Расход',
            requestName: 'expense'
        }
    };
    $scope.CategoryRequest = {
        name: null,
        operationType: null
    };

    $scope.loadCategories = function () {
        Category.getCategories().then(function (result) {
            $scope.categories = result;
        });
    };
    $scope.loadCategories();

    $scope.toggleShowCategoryFormFlag = function () {
        $scope.showCategoryForm = !$scope.showCategoryForm;
    };

    $scope.prepareToEdit = function (editable) {
        UtilService.markItemEditableFlag($scope.categories, editable, true);
        $scope.loadCategories();
    };

    $scope.closeErrorAlert = function (index) {
        $scope.errors.splice(index, 1);
    };

    $scope.saveCategory = function (form) {
        if (form && form.$valid) {
            Category.save($scope.CategoryRequest).then(function () {
                $scope.loadCategories();
                $scope.showCategoryForm = false;
                $scope.CategoryRequest = UtilService.resetFields($scope.CategoryRequest);
                form.$setPristine();
            }, function (error) {
                $scope.errors.push({message: UtilService.getExceptionMessage(error)});
            });
        }
    };

    $scope.edit = function (editable, form) {
        UtilService.markItemEditableFlag($scope.categories, editable, false);
        if (form && form.$valid) {
            Category.update(editable).then(function () {
                $scope.loadCategories();
            }, function (error) {
                $scope.errors.push({message: UtilService.getExceptionMessage(error)});
            });
        }
    };

    $scope.openDeleteCategory = function (removable) {
        var modalInstance = $modal.open({
            templateUrl: 'deleteDialog',
            controller: 'DialogController',
            size: 'sm'
        });
        modalInstance.result.then(function (confirmed) {
            if (confirmed) {
                Category.remove(removable).then(function () {
                    $scope.loadCategories();
                }, function (error) {
                    $scope.errors.push({message: UtilService.getExceptionMessage(error)});
                });
            }
        });
    };
});
