settingsModule.filter('operationType', function () {
    return function (input) {
        var result = "";
        if (input == "expense") {
            result = "Расход";
        } else if (input == "income") {
            result = "Доход";
        }
        return result;
    };
});