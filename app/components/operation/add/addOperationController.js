operationModule.controller('AddOperationController', function($scope, Account, Operation, Label, ComponentService, LocalStorageService, UtilService) {
    var operationType = LocalStorageService.getOperationType();

    var calcTitle = function() {
        if (operationType == 'income') {
            $scope.title = 'доход';
        }
        if (operationType == 'expense') {
            $scope.title = 'расход';
        }
    };
    calcTitle();

    $scope.errors = [];

    $scope.closeErrorAlert = function(index) {
        $scope.errors.splice(index, 1);
    };

    Label.getLabels(operationType).then(function(labels) {
        $scope.labels = labels;
    });

    Account.getAll().then(function(result) {
        $scope.accounts = result;
    });

    Operation.getUniqueDescriptions(operationType).then(function(descriptions) {
        $scope.descriptions = descriptions;
    });

    $scope.OperationRequest = {
        account: null,
        cost: null,
        description: null,
        date: null,
        labels: [],
        operationType: operationType
    };

    //TODO Походу не используется
    $scope.setLabel = function(label) {
        var cleanLabel = function(noisedLabel) {
            return {
                id: noisedLabel.id,
                name: noisedLabel.name,
                operationType: noisedLabel.operationType
            };
        };
        $scope.OperationRequest.label = cleanLabel(label);
    };

    $scope.save = function(operationForm) {
        if (operationForm && operationForm.$valid) {
            var tickedLabels = UtilService.getTickedItems($scope.labels);
            $scope.OperationRequest.labels = UtilService.cleanLabels(tickedLabels);
            Operation.save($scope.OperationRequest).then(function(savedOperation) {
                UtilService.setItemsUnticked($scope.labels);
                ComponentService.backHome();
            }, function(error) {
                $scope.errors.push({message: UtilService.getExceptionMessage(error)});
            });
        } else {
            $scope.errors.push({message: "Invalid operation form"});
        }
    };

    $scope.setToday = function () {
        $scope.OperationRequest.date = new Date();
    };

    $scope.getValidateClass = UtilService.getValidateClass;
});