operationModule.controller('EditOperationController', function($scope, $modal, Operation, Label, ComponentService, LocalStorageService, UtilService) {
    $scope.editable = LocalStorageService.getOperation();
    $scope.editable.date = new Date($scope.editable.date);
    $scope.categories = [];
    $scope.labels = [];
    $scope.errors = [];
    $scope.getPrefillingValidateClass = UtilService.getPrefillingValidateClass;

    $scope.isTransfer = function () {
        return $scope.editable.operationType == "transfer";
    };

    Operation.getUniqueDescriptions($scope.editable.operationType).then(function(descriptions) {
        $scope.descriptions = descriptions;
    });

    if (!$scope.isTransfer()) {
        Label.getLabels($scope.editable.operationType).then(function (result) {
            UtilService.markItemsAsTicked(result, $scope.editable.labels);
            $scope.labels = result;
        });       
    }

    $scope.edit = function(operationForm) {
        if (operationForm && operationForm.$valid) {
            if (!$scope.isTransfer()) {
                var tickedLabels = UtilService.getTickedItems($scope.labels);
                $scope.editable.labels = UtilService.cleanLabels(tickedLabels);
            }
            Operation.update($scope.editable).then(function () {
                UtilService.setItemsUnticked($scope.labels);
                ComponentService.backHome();
            }, function (error) {
                $scope.errors.push({message: UtilService.getExceptionMessage(error)});
            });
        }
    };

    $scope.openDeleteOperation = function() {
        var modalInstance = $modal.open({
            templateUrl: 'deleteOperation',
            controller: 'DialogController',
            size: 'sm'
        });
        modalInstance.result.then(function(confirmed) {
            if (confirmed) {
                Operation.remove($scope.editable).then(function() {
                    ComponentService.backHome();
                });
            }
        });
    };

    $scope.closeErrorAlert = function(index) {
        $scope.errors.splice(index, 1);
    };
});