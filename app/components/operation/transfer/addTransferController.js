operationModule.controller('AddTransferController', function ($scope, Account, Operation, ComponentService, LocalStorageService, UtilService) {
    $scope.getValidateClass = UtilService.getValidateClass;
    $scope.errors = [];

    $scope.closeErrorAlert = function (index) {
        $scope.errors.splice(index, 1);
    };

    Account.getAll().then(function (result) {
        $scope.accounts = result;
    });

    Operation.getUniqueDescriptions(LocalStorageService.getOperationType()).then(function (descriptions) {
        $scope.descriptions = descriptions;
    });

    $scope.TransferRequest = {
        from: null,
        to: null,
        cost: null,
        date: null,
        description: null,
        operationType: "transfer"
    };

    $scope.transfer = function (transferForm) {
        if (transferForm && transferForm.$valid) {
            Operation.transfer($scope.TransferRequest).then(function (savedOperations) {
                ComponentService.backHome();
            }, function (error) {
                $scope.errors.push({message: UtilService.getExceptionMessage(error)});
            });
        } else {
            $scope.errors.push({message: "Invalid transfer form"});
        }
    };

    $scope.setToday = function () {
        $scope.TransferRequest.date = new Date();
    };
});