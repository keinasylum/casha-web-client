var mainModule = angular.module('main', ['ui.bootstrap', 'ngTable']);

mainModule.controller('MainController', function ($scope, $filter, Account, Operation, ComponentService, ngTableParams, UtilService, Statistic) {
    $scope.addOperation = ComponentService.addOperation;
    $scope.addTransfer = ComponentService.addTransfer;
    $scope.editOperation = ComponentService.editOperation;

    Account.getAll().then(function (result) {
        $scope.accounts = result;
    });

    Account.getBalance().then(function (result) {
        $scope.balance = result;
    });

    var start = $filter('date')(new Date().setDate(1), 'yyyy-MM-dd HH:mm:ss'); 
    var end = $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss');
    Statistic.getIncomeExpence(start, end).then(function (data) {
        $scope.income = data.income;
        $scope.expense = data.expense;
        $scope.difference = (data.income - data.expense);
    }, function (error) {
        $scope.errors.push(UtilService.getException(error));
    });

    if (UtilService.isMobileDevice()) {
        Operation.getLastOperations(10, "DESC").then(function (result) {
            $scope.operations = result;
        });
    } else {
        Operation.getAll().then(function (result) {
            $scope.operations = result;
            $scope.operationsTable = new ngTableParams({
                page: 0
            }, {
                groupBy: function (item) {
                    var filtered = $filter('date')(item.date, "short");
                    return UtilService.transformDate(filtered);
                },
                getData: function ($defer, params) {
                    params.total($scope.operations.length);
                    $defer.resolve($scope.operations.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    if (params.page() == 0) {
                        var elementsOnPage = 25;
                        params.count(elementsOnPage);
                        params.page(Math.ceil($scope.operations.length / elementsOnPage));
                    }
                }
            });
        });
    }
});
