statisticModule.controller('IncomeExpenseChartController', function ($scope, $filter, Statistic, UtilService) {
    $scope.IncomeExpense = {
        start: new Date(),
        end: new Date(),
        income: 0,
        expense: 0
    };

    $scope.message = "Выбери диапазон дат для отображения графика";

    $scope.refreshIncomeExpenseData = function () {
        var start = $filter('date')($scope.IncomeExpense.start, "yyyy-MM-dd HH:mm:ss");
        var end = $filter('date')($scope.IncomeExpense.end, "yyyy-MM-dd HH:mm:ss");
        Statistic.getIncomeExpence(start, end).then(function (data) {
            $scope.data = [data.income, data.expense];
            $scope.labels = ["Доход", "Расход"];
            $scope.message = "Доход: " + data.income + ", расход: " + data.expense + ", разница: " + (data.income - data.expense);
        }, function (error) {
            $scope.errors.push(UtilService.getException(error));
        });
    };

    $scope.closeErrorAlert = function (index) {
        $scope.errors.splice(index, 1);
    };

    $scope.getValidateClass = UtilService.getValidateClass;
});