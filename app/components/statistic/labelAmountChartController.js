statisticModule.controller('LabelAmountChartController', function ($scope, $filter, Statistic, UtilService, Label) {
    $scope.message = "Выбери метку и диапазон дат для отображения графика";
    $scope.LabelAmount = {
        start: new Date(),
        end: new Date(),
        label: null
    };

    Label.getLabels().then(function (labels) {
        $scope.labels = labels;
    });

    $scope.refreshLabelAmount = function () {
        var start = $filter('date')($scope.LabelAmount.start, 'yyyy-MM-dd');
        var end = $filter('date')($scope.LabelAmount.end, 'yyyy-MM-dd');
        Statistic.labelAmount(start, end, $scope.LabelAmount.label).then(function (data) {
            $scope.message = "Сумма: " + data.sum;
            $scope.labels = data.labels;
            $scope.series = [$scope.LabelAmount.label.name];
            $scope.data = [data.data];
        }, function (error) {
            $scope.errors.push(UtilService.getException(error));
        });
    };

    $scope.setLabel = function (label) {
        var cleanLabel = function (noisedLabel) {
            return {
                id: noisedLabel.id,
                name: noisedLabel.name,
                operationType: noisedLabel.operationType
            };
        };
        $scope.LabelAmount.label = cleanLabel(label);
    };

    $scope.closeErrorAlert = function (index) {
        $scope.errors.splice(index, 1);
    };

    $scope.getValidateClass = UtilService.getValidateClass;
});