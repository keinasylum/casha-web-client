'use strict';

var casha = angular.module('casha', [
    'ngRoute',
    'ngTouch',
    'main',
    'settings',
    'operation',
    'history',
    'statistic',
    'util',
    'resource',
    'communication',
    'dialog'
], function ($routeProvider, $locationProvider) {
    $routeProvider.when('/casha', {templateUrl: 'components/main/main'});
    $routeProvider.when('/casha/addOperation', {templateUrl: 'components/operation/add/addOperation'});
    $routeProvider.when('/casha/addTransfer', {templateUrl: 'components/operation/transfer/addTransfer'});
    $routeProvider.when('/casha/editOperation', {templateUrl: 'components/operation/edit/editOperation'});
    $routeProvider.when('/casha/balanceHistory', {templateUrl: 'components/history/balanceHistory'});
    $routeProvider.when('/casha/statistic', {templateUrl: 'components/statistic/statistic'});
    $routeProvider.when('/casha/settings', {templateUrl: 'components/settings/settings'});
    $routeProvider.otherwise({redirectTo: '/casha'});
    $locationProvider.html5Mode(true);
});

casha.constant('URLs', {
    baseURL: 'http://localhost:8080/casha',
    totalBalance: '/account/totalBalance',
    uniqueDescriptionIncome: '/abstractOperation/uniqueDescription?operationType=income',
    uniqueDescriptionExpense: '/abstractOperation/uniqueDescription?operationType=expense',
    uniqueDescriptionTransfer: '/abstractOperation/uniqueDescription?operationType=transfer',
    allUniqueDescriptions: '/abstractOperation/allUniqueDescription'
});

casha.controller('HeaderController', function ($location) {
    var self = this;

    self.isActive = function (viewLocation) {
        return viewLocation == $location.path();
    };
});