communicationModule.service('ComponentService', function($q, $location, LocalStorageService) {
    var self = this;

    self.addOperation = function(operationType) {
        LocalStorageService.setOperationType(operationType);
        $location.path('/casha/addOperation');
    };

    self.addTransfer = function(operationType) {
        LocalStorageService.setOperationType(operationType);
        $location.path('/casha/addTransfer');
    };

    self.editOperation = function(operation) {
        LocalStorageService.setOperation(operation);
        $location.path('/casha/editOperation');
    };

    self.backHome = function() {
        $location.path('/casha');
    };

    return self;
});