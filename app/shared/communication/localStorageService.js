communicationModule.factory('LocalStorageService', function(localStorageService, $log) {
    var self = this;

    self.setOperationType = function(type) {
        if (isSupported()) {
            if (validateOperationType(type)) {
                localStorageService.set('operationType', type);
            } else {
                $log.error("LocalStorageService.setOperationType: Set wrong operation type");
            }
        }
    };

    self.getOperationType = function() {
        if (isSupported()) {
            var operationType = localStorageService.get('operationType');
            if (operationType) {
                return operationType;
            } else {
                $log.error("LocalStorageService.getOperationType: OperationType is not set");
                return null;
            }
        }
    };

    var validateOperationType = function(type) {
        var types = ['income', 'expense', 'transfer'];
        if (type) {
            for (var i in types) {
                if (types[i] == type) {
                    return true;
                }
            }
        }
        return false;
    };

    var isSupported = function() {
        if (localStorageService.isSupported) {
            return true;
        } else {
            $log.error("LocalStorageService.isSupported: Local storage not supported");
            return false;
        }
    };

    self.setOperation = function(operation) {
        if (isSupported) {
            if (operation) {
                localStorageService.set('operation', operation);
            } else {
                $log.error("LocalStorageService.setOperation: Operation can not be null");
            }
        }
    };

    self.getOperation = function() {
        if (isSupported) {
            var operation = localStorageService.get('operation');
            if (operation) {
                return operation;
            } else {
                $log.error("LocalStorageService.getOperation: Operation is not set");
            }
        } else {
            $log.error("LocalStorageService.getOperation: Local storage not supported");
            return null;
        }
    };

    return self;
});