var utilModule = angular.module('util', []);

utilModule.factory('UtilService', function ($log) {
    var self = this;

    self.getValidateClass = function (form, field) {
        if (form && form[field]) {
            return {
                'form-group has-error': form[field].$invalid && !form[field].$pristine,
                'form-group has-success': form[field].$valid,
                'form-group': form[field].$pristine && form[field].$invalid
            }
        }
        return null;
    };

    self.getPrefillingValidateClass = function (form, field) {
        if (form && form[field]) {
            return {
                'form-group has-error': form[field].$invalid && !form[field].$pristine,
                'form-group has-success': form[field].$valid && !form[field].$pristine,
                'form-group': form[field].$valid
            }
        }
        return null;
    };

    self.getExceptionMessage = function (exception) {
        var errors = [];
        if (exception && exception.data) {
            var exData = exception.data;
            if (angular.isArray(exData)) {
                angular.forEach(exData, function (value) {
                    errors.push(value.defaultMessage);
                });
            } else {
                if (exData.rootCause) {
                    errors.push(exData.rootCause.message);
                } else {
                    errors.push(exData.message);
                }
            }
        } else {
            errors.push("Error, see console");
        }
        return errors;
    };

    self.getException = function (exception) {
        var error = {
            message: ""
        };
        if (exception && exception.data) {
            var exData = exception.data;
            if (angular.isArray(exData)) {
                angular.forEach(exData, function (value, key) {
                    error.message += value.defaultMessage + " ";
                });
            } else {
                if (exData.rootCause) {
                    error.message = exData.rootCause.message;
                } else {
                    error.message = exData.message;
                }
            }
        } else {
            error.message = "Error, see console";
        }
        return error;
    };

    self.markItemEditableFlag = function (array, item, flag) {
        if (angular.isArray(array) && angular.isObject(item) && angular.isDefined(flag)) {
            angular.forEach(array, function (value, key) {
                if (value.id == item.id) {
                    value['$$editable'] = flag;
                    array[key] = value;
                }
            });
        } else {
            $log.error("UtilService.markItemEditableFlag: invalid arguments");
        }
    };

    self.isEdit = function (item) {
        if (item.$$editable) {
            return true
        }
        return false;
    };

    self.markItemMergeableFlag = function (array, item) {
        var markMergeable = function (value, key) {
            value['$$mergeable'] = !value['$$mergeable'];
            array[key] = value;
        };
        if (angular.isArray(array) && angular.isObject(item)) {
            angular.forEach(array, function (value, key) {
                if (value.id && value.id == item.id) {
                    markMergeable(value, key);
                } else if (value.name && value.name == item.name) {
                    markMergeable(value, key);
                }
            });
        } else {
            $log.error("UtilService.markItemMergeableFlag: invalid arguments");
        }
    };

    self.getMergeableItems = function (array) {
        if (angular.isArray(array)) {
            var result = [];
            angular.forEach(array, function (value) {
                if (value['$$mergeable']) {
                    result.push(value);
                }
            });
            return result;
        } else {
            $log.error("UtilService.getMergeableItems: invalid arguments");
            return null;
        }
    };

    self.resetFields = function (object) {
        if (angular.isObject(object)) {
            angular.forEach(object, function (value, key) {
                object[key] = null;
            });
            return object;
        } else {
            $log.error("UtilService.getMergeableItems: invalid arguments");
            return null;
        }
    };

    self.resetField = function (array, field) {
        if (angular.isArray(array) && field != null) {
            angular.forEach(array, function (value) {
                if (value[field] != null) {
                    value[field] == null;
                }
            });
        } else {
            $log.error("UtilService.resetField: invalid arguments");
            return null;
        }
    };

    self.isMobileDevice = function () {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            return true;
        } else {
            return false;
        }
    };

    self.transformDate = function (strDate) {
        var month = [];
        month[0] = "Январь";
        month[1] = "Февраль";
        month[2] = "Март";
        month[3] = "Апрель";
        month[4] = "Май";
        month[5] = "Июнь";
        month[6] = "Июль";
        month[7] = "Август";
        month[8] = "Сентябрь";
        month[9] = "Октябрь";
        month[10] = "Ноябрь";
        month[11] = "Декабрь";
        var date = new Date(strDate);
        var year = date.getFullYear();
        return month[date.getMonth()] + ' ' + year;
    };

    self.getModelIndex = function (array, serchable) {
        var index = null;
        if (angular.isArray(array) && angular.isDefined(serchable) && serchable != null) {
            angular.forEach(array, function (value, key) {
                if (value.id == serchable.id) {
                    index = key;
                }
            });
        } else {
            $log.error("UtilService.getModelIndex: invalid arguments");
        }
        return index;
    };

    self.cleanCategory = function (category) {
        if (category != null) {
            return {
                id: category.id,
                name: category.name,
                operationType: category.operationType
            }
        } else {
            $log.error("UtilService.cleanCategory: invalid arguments");
        }
    };

    self.cleanCategories = function (categories) {
        var result = [];
        if (angular.isArray(categories)) {
            angular.forEach(categories, function (value) {
                result.push(self.cleanCategory(value));
            });
        } else {
            $log.error("UtilService.cleanCategories: invalid arguments");
        }
        return result;
    };

    self.cleanLabels = function (labels) {
        var result = [];
        angular.forEach(labels, function (label) {
            result.push({
                id: label.id,
                name: label.name,
                operationType: label.operationType
            });
        });
        return result;
    };

    self.getTickedItems = function (array) {
        var result = [];
        if (angular.isArray(array)) {
            angular.forEach(array, function (value) {
                if (value.ticked) {
                    result.push(value);
                }
            });
        } else {
            $log.error("UtilService.getTickedItems: invalid arguments");
        }
        return result;
    };

    self.setItemsUnticked = function (array) {
        if (angular.isArray(array)) {
            angular.forEach(array, function (value) {
                if (value.ticked) {
                    value.ticked = false;
                }
            });
        } else {
            $log.error("UtilService.setItemsUnticked: invalid arguments");
        }
    };

    self.markItemsAsTicked = function (array, tickedItems) {
        if (angular.isArray(array) && angular.isArray(tickedItems)) {
            angular.forEach(array, function (value) {
                angular.forEach(tickedItems, function (mustTicked) {
                    if (value.id == mustTicked.id) {
                        value["ticked"] = true;
                    }
                });
            });
        } else {
            $log.error("UtilService.markItemsAsTicked: invalid arguments");
        }
    };

    return self;
});