var dialogModule = angular.module('dialog', ['ui.bootstrap']);

dialogModule.controller('DialogController', function($scope, $modalInstance) {
    $scope.confirm = function(answer) {
        $modalInstance.close(answer);
    };
});
