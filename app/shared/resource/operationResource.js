resourceModule.service('Operation', function ($q, $resource, $http, URLs, $log, UtilService, BalanceHistory) {
    var self = this;

    var OperationResource = $resource(URLs.baseURL + '/operation/:serverParam?start=:start&end=:end&labelId=:labelId&direction=:direction&limit=:limit',
        {
            direction: '@direction',
            limit: '@limit'
        }, {
            getAll: {method: 'GET', params: {serverParam: 'all'}, isArray: true},
            update: {method: 'PUT'},
            getByDateAndLabel: {method: 'GET', params: {serverParam: 'getByDateAndLabel'}, isArray: true},
            getLastOperations: {method: 'GET', params: {serverParam: 'getLastOperations'}, isArray: true}
        });

    var AbstractOperationResource = $resource(URLs.baseURL + '/abstractOperation/:serverParam', {}, {
        getAll: {method: 'GET', params: {serverParam: 'all'}, isArray: true}
    });

    var TransferResource = $resource(URLs.baseURL + '/transfer/:serverParam.json', {}, {
        update: {method: 'PUT'}
    });

    var operations = [];
    var uniqueDescriptionIncome = [];
    var uniqueDescriptionExpense = [];
    var uniqueDescriptionTransfer = [];
    var allUniqueDescriptions = [];

    self.getAll = function () {
        var deferred = $q.defer();
        if (operations.length == 0) {
            AbstractOperationResource.getAll(function (data) {
                operations = data;
                deferred.resolve(operations);
            }, function (error) {
                $log.error("Operation.getAll: ", error);
                deferred.reject(error);
            });
        } else {
            deferred.resolve(operations);
        }

        return deferred.promise;
    };

    self.getLastOperations = function (limit, direction) {
        var deferred = $q.defer();
        if (operations.length == 0) {
            OperationResource.getLastOperations({limit: limit, direction: direction}, function (data) {
                operations = data;
                deferred.resolve(operations);
            }, function (error) {
                $log.error("Operation.getLastOperations: ", error);
                deferred.reject(error);
            });
        } else {
            deferred.resolve(operations);
        }
        return deferred.promise;
    };

    self.save = function (operationRequest) {
        var deferred = $q.defer();
        OperationResource.save(operationRequest, function (savedOperation) {
            operations.push(savedOperation);
            BalanceHistory.getByOperationId(savedOperation.id);
            deferred.resolve(savedOperation);
        }, function (error) {
            $log.error("Operation.save: ", error);
            deferred.reject(error);
        });
        return deferred.promise;
    };

    self.transfer = function (transferRequest) {
        var deferred = $q.defer();
        TransferResource.save(transferRequest, function (savedOperation) {
            operations.push(savedOperation);
            BalanceHistory.getByOperationId(savedOperation.id);
            deferred.resolve(savedOperation);
        }, function (error) {
            $log.error("Operation.transfer: ", error);
            deferred.reject(error);
        });
        return deferred.promise;
    };

    self.update = function (updated) {
        var precess = function (updatedResponse) {
            BalanceHistory.getByOperationId(updatedResponse.id);
            var index = UtilService.getModelIndex(operations, updatedResponse);
            operations[index] = updatedResponse;
            deferred.resolve(updatedResponse);
        };
        var deferred = $q.defer();
        if (updated.operationType == 'transfer') {
            TransferResource.update(updated, function (updatedTransfer) {
                precess(updatedTransfer);
            }, function (error) {
                $log.error("Operation.update: ", error);
                deferred.reject(error);
            });
        } else {
            OperationResource.update(updated, function (updatedOperation) {
                precess(updatedOperation);
            }, function (error) {
                $log.error("Operation.update: ", error);
                deferred.reject(error);
            });
        }
        return deferred.promise;
    };

    self.remove = function (removable) {
        var precess = function (removedResponse) {
            BalanceHistory.getByOperationId(removedResponse.id);
            var index = UtilService.getModelIndex(operations, removedResponse);
            operations.splice(index, 1);
            deferred.resolve(removedResponse);
        };
        var deferred = $q.defer();
        if (removable.operationType == 'transfer') {
            TransferResource.remove({serverParam: removable.id}, function (removed) {
                precess(removed);
            }, function (error) {
                $log.error("Operation.remove: ", error);
                deferred.reject(error);
            });
        } else {
            OperationResource.remove({serverParam: removable.id}, function (removed) {
                precess(removed);
            }, function (error) {
                $log.error("Operation.remove: ", error);
                deferred.reject(error);
            });
        }
        return deferred.promise;
    };

    self.getUniqueDescriptions = function (type) {
        var deferred = $q.defer();
        if (type) {
            if (type == 'income') {
                if (uniqueDescriptionIncome.length == 0) {
                    $http.get(URLs.baseURL + URLs.uniqueDescriptionIncome).success(function (data) {
                        uniqueDescriptionIncome = data;
                        deferred.resolve(uniqueDescriptionIncome);
                    }, function (error) {
                        $log.error("Operation.getUniqueDescriptions: ", error);
                        deferred.reject(error);
                    });
                } else {
                    deferred.resolve(uniqueDescriptionIncome);
                }
            }
            if (type == 'expense') {
                if (uniqueDescriptionExpense.length == 0) {
                    $http.get(URLs.baseURL + URLs.uniqueDescriptionExpense).success(function (data) {
                        uniqueDescriptionExpense = data;
                        deferred.resolve(uniqueDescriptionExpense);
                    }, function (error) {
                        $log.error("Operation.getUniqueDescriptions: ", error);
                        deferred.reject(error);
                    });
                } else {
                    deferred.resolve(uniqueDescriptionExpense);
                }
            }
            if (type == 'transfer') {
                if (uniqueDescriptionTransfer.length == 0) {
                    $http.get(URLs.baseURL + URLs.uniqueDescriptionTransfer).success(function (data) {
                        uniqueDescriptionTransfer = data;
                        deferred.resolve(uniqueDescriptionTransfer);
                    }, function (error) {
                        $log.error("Operation.getUniqueDescriptions: ", error);
                        deferred.reject(error);
                    });
                } else {
                    deferred.resolve(uniqueDescriptionTransfer);
                }
            }
        } else {
            if (allUniqueDescriptions.length == 0) {
                $http.get(URLs.baseURL + URLs.allUniqueDescriptions).success(function (data) {
                    allUniqueDescriptions = data;
                    deferred.resolve(allUniqueDescriptions);
                }, function (error) {
                    $log.error("Operation.getUniqueDescriptions: ", error);
                    deferred.reject(error);
                });
            } else {
                deferred.resolve(allUniqueDescriptions);
            }
        }
        return deferred.promise;
    };

    return self;
});