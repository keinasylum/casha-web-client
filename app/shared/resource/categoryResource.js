resourceModule.service('Category', function ($q, UtilService, $log, $resource, URLs) {
    var self = this;

    var CategoryResource = $resource(URLs.baseURL + '/category/:serverParam?typeName=:type', {}, {
        getAll: {method: 'GET', params: {serverParam: 'all'}, isArray: true},
        getIncome: {method: 'GET', params: {serverParam: 'byOperationType', type: 'income'}, isArray: true},
        getExpense: {method: 'GET', params: {serverParam: 'byOperationType', type: 'expense'}, isArray: true},
        update: {method: 'PUT'}
    });

    var categories = [];
    var categoriesIncome = [];
    var categoriesExpense = [];

    self.getCategories = function (operationType) {
        var deferred = $q.defer();
        if (operationType) {
            if (operationType == 'income') {
                getIncome().then(function (result) {
                    deferred.resolve(result);
                });
            } else if (operationType == 'expense') {
                getExpense().then(function (result) {
                    deferred.resolve(result);
                });
            } else {
                $log.error("Category.getCategories: Wrong operationType");
                deferred.reject(null);
            }
        } else {
            getAll().then(function (result) {
                deferred.resolve(result);
            });
        }
        return deferred.promise;
    };

    var getAll = function () {
        var deferred = $q.defer();
        if (categories.length == 0) {
            CategoryResource.getAll(function (data) {
                categories = data;
                deferred.resolve(categories);
            }, function (error) {
                $log.error("Category.getAll: ", error);
                deferred.reject(error);
            });
        } else {
            deferred.resolve(categories);
        }
        return deferred.promise;
    };

    var getIncome = function () {
        var deferred = $q.defer();
        if (categoriesIncome.length == 0) {
            CategoryResource.getIncome(function (data) {
                categoriesIncome = data;
                deferred.resolve(categoriesIncome);
            }, function (error) {
                $log.error("Category.getIncome: ", error);
                deferred.reject(error);
            });
        } else {
            deferred.resolve(categoriesIncome);
        }
        return deferred.promise;
    };

    var getExpense = function () {
        var deferred = $q.defer();
        if (categoriesExpense.length == 0) {
            CategoryResource.getExpense(function (data) {
                categoriesExpense = data;
                deferred.resolve(categoriesExpense);
            }, function (error) {
                $log.error("Category.getExpense: ", error);
                deferred.reject(error);
            });
        } else {
            deferred.resolve(categoriesExpense);
        }
        return deferred.promise;
    };

    self.save = function (categoryRequest) {
        var deferred = $q.defer();
        CategoryResource.save(categoryRequest, function (savedCategory) {
            categories.push(savedCategory);
            deferred.resolve(savedCategory);
        }, function (error) {
            $log.error("Category.save: ", error);
            deferred.reject(error);
        });
        return deferred.promise;
    };

    self.update = function (updated) {
        var deferred = $q.defer();
        CategoryResource.update(updated, function (updated) {
            var index = UtilService.getModelIndex(categories, updated);
            categories[index] = updated;
            deferred.resolve(updated);
        }, function (error) {
            $log.error("Category.update: ", error);
            deferred.reject(error);
        });
        return deferred.promise;
    };

    self.remove = function (removable) {
        var deferred = $q.defer();
        CategoryResource.remove({serverParam: removable.id}, function (removed) {
            var index = UtilService.getModelIndex(categories, removed);
            categories.splice(index, 1);
            if (removed.operationType == "income") {
                index = UtilService.getModelIndex(categoriesIncome, removed);
                categoriesIncome.splice(index, 1);
            } else if (removed.operationType == "expense") {
                index = UtilService.getModelIndex(categoriesExpense, removed);
                categoriesExpense.splice(index, 1);
            }
            deferred.resolve(removed);
        }, function (error) {
            $log.error("Category.remove: ", error);
            deferred.reject(error);
        });
        return deferred.promise;
    };

    return self;
});