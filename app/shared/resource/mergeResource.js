resourceModule.service('Merge', function ($q, $log, $resource, URLs) {
    var self = this;

    var MergeResource = $resource(URLs.baseURL + '/merging/:serverMethod', {}, {
        mergeLabels: {method: 'POST', params: {serverMethod: 'labels'}},
        mergeDescriptions: {method: 'POST', params: {serverMethod: 'descriptions'}}
    });

    self.mergeLabels = function (mergeRequest) {
        var deferred = $q.defer();
        MergeResource.mergeLabels(mergeRequest, function (mergedLabel) {
            deferred.resolve(mergedLabel);
        }, function (error) {
            $log.error("Merge.mergeLabels: ", error);
            deferred.reject(error);
        });
        return deferred.promise;
    };

    self.mergeDescriptions = function (mergeRequest) {
        var deferred = $q.defer();
        MergeResource.mergeDescriptions(mergeRequest, function () {
            deferred.resolve();
        }, function (error) {
            $log.error("Merge.mergeDescriptions: ", error);
            deferred.reject(error);
        });
        return deferred.promise;
    };

    return self;
});
