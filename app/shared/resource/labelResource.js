resourceModule.service('Label', function ($q, $log, UtilService, $resource, URLs) {
    var self = this;

    var LabelResource = $resource(URLs.baseURL + '/label/:serverParam?typeName=:type', {}, {
        getAll: {method: 'GET', params: {serverParam: 'all'}, isArray: true},
        getIncome: {method: 'GET', params: {serverParam: 'byOperationType', type: 'income'}, isArray: true},
        getExpense: {method: 'GET', params: {serverParam: 'byOperationType', type: 'expense'}, isArray: true},
        update: {method: 'PUT'}
    });

    var labels = [];
    var labelsIncome = [];
    var labelsExpense = [];

    self.getLabels = function (operationType) {
        var deferred = $q.defer();
        if (operationType) {
            if (operationType == 'income') {
                getIncome().then(function (result) {
                    deferred.resolve(result);
                });
            } else if (operationType == 'expense') {
                getExpense().then(function (result) {
                    deferred.resolve(result);
                });
            } else {
                $log.error("Label.getLabels: Wrong operationType");
                deferred.reject(null);
            }
        } else {
            getAll().then(function (result) {
                deferred.resolve(result);
            });
        }
        return deferred.promise;
    };

    self.save = function (labelRequest) {
        var deferred = $q.defer();
        LabelResource.save(labelRequest, function (savedLabel) {
            labels.push(savedLabel);
            if (savedLabel.operationType == 'income') {
                labelsIncome.push(savedLabel);
            } else if (savedLabel.operationType == 'expense') {
                labelsExpense.push(savedLabel);
            }
            deferred.resolve(savedLabel);
        }, function (error) {
            $log.error("Label.save: ", error);
            deferred.reject(error);
        });
        return deferred.promise;
    };

    self.update = function (updated) {
        var deferred = $q.defer();
        LabelResource.update(updated, function (updated) {
            var index = UtilService.getModelIndex(labels, updated);
            labels[index] = updated;
            if (updated.operationType == 'income') {
                index = UtilService.getModelIndex(labelsIncome, updated);
                labelsIncome[index] = updated;
            } else if (updated.operationType == 'expense') {
                index = UtilService.getModelIndex(labelsExpense, updated);
                labelsExpense[index] = updated;
            }
            deferred.resolve(updated);
        }, function (error) {
            $log.error("Label.update: ", error);
            deferred.reject(error);
        });
        return deferred.promise;
    };

    self.remove = function (removable) {
        var deferred = $q.defer();
        LabelResource.remove({serverParam: removable.id}, function (removed) {
            var index = UtilService.getModelIndex(labels, removed);
            labels.splice(index, 1);
            if (removed.operationType == 'income') {
                index = UtilService.getModelIndex(labelsIncome, removed);
                labelsIncome.splice(index, 1);
            } else if (removed.operationType == 'expense') {
                index = UtilService.getModelIndex(labelsExpense, removed);
                labelsExpense.splice(index, 1);
            }
            deferred.resolve(removed);
        }, function (error) {
            $log.error("Label.remove: ", error);
            deferred.reject(error);
        });
        return deferred.promise;
    };

    var getIncome = function () {
        var deferred = $q.defer();
        if (labelsIncome.length == 0) {
            LabelResource.getIncome(function (data) {
                labelsIncome = data;
                deferred.resolve(labelsIncome);
            }, function (error) {
                $log.error("Label.getIncome: ", error);
                deferred.reject(error);
            });
        } else {
            deferred.resolve(labelsIncome);
        }
        return deferred.promise;
    };

    var getExpense = function () {
        var deferred = $q.defer();
        if (labelsExpense.length == 0) {
            LabelResource.getExpense(function (data) {
                labelsExpense = data;
                deferred.resolve(labelsExpense);
            }, function (error) {
                $log.error("Label.getExpense: ", error);
                deferred.reject(error);
            });
        } else {
            deferred.resolve(labelsExpense);
        }
        return deferred.promise;
    };

    var getAll = function () {
        var deferred = $q.defer();
        if (labels.length == 0) {
            LabelResource.getAll(function (data) {
                labels = data;
                deferred.resolve(labels);
            }, function (error) {
                $log.error("Label.getAll: ", error);
                deferred.reject(error);
            });
        } else {
            deferred.resolve(labels);
        }
        return deferred.promise;
    };

    return self;
});