resourceModule.service('Account', function ($q, $http, URLs, $log, UtilService, $resource) {
    var self = this;

    var AccountResource = $resource(URLs.baseURL + '/account/:serverParam.json', {}, {
        getAll: {method: 'GET', params: {serverParam: 'all'}, isArray: true},
        update: {method: 'PUT'}
    });

    var accounts = [];
    var balance = 0;

    self.getAll = function () {
        var deferred = $q.defer();
        AccountResource.getAll(function (data) {
            accounts = data;
            deferred.resolve(accounts);
        }, function (error) {
            $log.error("Account.getAll: ", error);
            deferred.reject(error);
        });
        return deferred.promise;
    };

    self.getBalance = function () {
        var deferred = $q.defer();
        $http.get(URLs.baseURL + URLs.totalBalance).success(function (data) {
            balance = data;
            deferred.resolve(balance);
        }, function (error) {
            $log.error("Account.getBalance: ", error);
            deferred.reject(error);
        });
        return deferred.promise;
    };

    self.save = function (accountRequest) {
        var deferred = $q.defer();
        AccountResource.save(accountRequest, function (savedAccount) {
            accounts.push(savedAccount);
            deferred.resolve(savedAccount);
        }, function (error) {
            $log.error("Account.save: ", error);
            deferred.reject(error);
        });
        return deferred.promise;
    };

    self.update = function (updated) {
        var deferred = $q.defer();
        AccountResource.update(updated, function (updated) {
            var index = UtilService.getModelIndex(accounts, updated);
            accounts[index] = updated;
            deferred.resolve(updated);
        }, function (error) {
            $log.error("Account.update: ", error);
            deferred.reject(error);
        });
        return deferred.promise;
    };

    self.remove = function (removable) {
        var deferred = $q.defer();
        AccountResource.remove({serverParam: removable.id}, function (removed) {
            var index = UtilService.getModelIndex(accounts, removed);
            accounts.splice(index, 1);
            deferred.resolve(removed);
        }, function (error) {
            $log.error("Account.remove: ", error);
            deferred.reject(error);
        });
        return deferred.promise;
    };

    return self;
});