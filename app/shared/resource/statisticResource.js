resourceModule.service('Statistic', function ($q, $resource, $log, $filter, URLs) {
    var self = this;

    var StatisticResource = $resource(URLs.baseURL + '/statistic/:serverMethod?start=:start&end=:end&labelId=:labelId', {}, {
        accountInfo: {method: 'GET', params: {serverMethod: 'accountInfo'}, isArray: true}
    });

    self.getIncomeExpence = function (start, end) {
        var deferred = $q.defer();
        StatisticResource.get({ serverMethod: 'incomeExpense', start: start, end: end }, function(data) {
            deferred.resolve(data);
        }, function(error) {
            $log.error("Statistic.getIncomeExpence: ", error);
            deferred.reject(error);
        });
        return deferred.promise;
    };

    self.labelAmount = function (start, end, label) {
        var deferred = $q.defer();
        if (angular.isDate(start) || angular.isDate(end) || label) {
            StatisticResource.get({ serverMethod: 'labelAmount', start: start, end: end, labelId: label.id }, function(response) {
                var data = [];
                var labels = [];
                angular.forEach(response.dateAmounts, function (value, key) {
                    data.push(value.amount);
                    labels.push($filter('date')(value.date, 'yyyy-MM-dd'));
                });
                deferred.resolve({
                    data: data,
                    labels: labels,
                    sum: response.sum
                });
            }, function(error) {
                $log.error("Statistic.labelAmount: ", error);
                deferred.reject(error);
            });
        } else {
            $log.error("Statistic.labelAmount: wrong arguments");
            deferred.reject();
        }
        return deferred.promise;
    };

    return self;
});
