resourceModule.service('BalanceHistory', function ($q, $resource, $log, URLs) {
    var self = this;

    var BalanceResource = $resource(URLs.baseURL + '/balanceHistory/:serverParam/:operationId', {
        operationId: '@operationId'
    }, {
        getAll: {method: 'GET', params: {serverParam: 'all'}, isArray: true},
        getByOperationId: {method: 'GET', params: {serverParam: 'getByOperationId'}, isArray: true}
    });

    var balanceHistories = [];

    self.getAll = function () {
        var deferred = $q.defer();
        if (balanceHistories.length == 0) {
            BalanceResource.getAll(function (data) {
                balanceHistories = data;
                deferred.resolve(balanceHistories);
            }, function (error) {
                $log.error("BalanceHistory.getAll: ", error);
                deferred.reject(error);
            });
        } else {
            deferred.resolve(balanceHistories);
        }
        return deferred.promise;
    };

    self.getByOperationId = function (operationId) {
        var deferred = $q.defer();
        BalanceResource.getByOperationId({operationId: operationId}, function (array) {
            angular.forEach(array, function (balanceHistory) {
                balanceHistories.push(balanceHistory);
            });
            deferred.resolve(array);
        });
        return deferred.promise;
    };

    return self;
});