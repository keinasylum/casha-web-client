module.exports = function (config) {
  'use strict';
  config.set({
    basePath: '../',
    frameworks: ['jasmine'],
    singleRun: false,
    browsers: ['PhantomJS'],
    files: [
        'bower_components/angular/angular.js',
        'bower_components/angular-mocks/angular-mocks.js',
        'app/scripts/script.js',
        'test/spec/**/*.js'
    ]
  });
}