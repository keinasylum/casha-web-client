describe('sorting the list of users', function() {
  it('sorts in descending order by default', function() {
    var sorted = ['jeff', 'jack', 'igor'];
    expect(sorted).toEqual(['jeff', 'jack', 'igor']);
  });
});