module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-bg-shell');
    grunt.loadNpmTasks('grunt-bower-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-concurrent");
    grunt.loadNpmTasks("grunt-contrib-jade");
    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks('grunt-karma');
    grunt.initConfig({
        bgShell: {
            runNode: {
                cmd: 'node app.js',
                bg: false
            }
        },
        jade: {
            dest: {
                files: [{
                    expand: "true",
                    cwd: "./app/",
                    src: ["**/*.jade"],
                    dest: "./dist/",
                    ext: ".html"
                }]
            },
            options: {
                pretty: true,
                amd: false,
                compileDebug: false
            }
        },
        concat: {
            options: {
                sourceMap: true
            },
            initialization: {
                src: ['app/app.js', 'app/**/*.module.js'],
                dest: 'dist/stuff/script.js'
            },
            sources: {
                src: ['dist/stuff/script.js', 'app/**/*.js', '!app/app.js', '!app/**/*.module.js'],
                dest: 'dist/stuff/script.js'
            }
        },
        bower_concat: {
            all: {
                dest: 'dist/stuff/libs.js',
                cssDest: 'dist/stuff/libs.css',
                bowerOptions: {
                    relative: false
                }
            }
        },
        copy: {
            dist: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: ['bower_components/bootstrap/dist/fonts/*'],
                        dest: 'dist/fonts',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: true,
                        src: [
                            'assets/lib/angular-multi-select.js',
                            'assets/lib/angular-multi-select.css',
                            'bower_components/bootstrap/dist/css/bootstrap.css.map',
                            'bower_components/ng-table/dist/ng-table.min.js.map'
                        ],
                        dest: 'dist/stuff',
                        filter: 'isFile'
                    }
                ]
            }
        },
        uglify: {
            libs: {
                files: {
                    'dist/stuff/libs.min.js': [
                        'dist/stuff/libs.js', '' +
                        'dist/stuff/angular-multi-select.js'
                    ]
                }
            },
            sources: {
                files: {
                    'dist/stuff/script.min.js': [
                        'dist/stuff/script.js'
                    ]
                }
            }
        },
        cssmin: {
            libs: {
                files: {
                    'dist/stuff/libs.min.css': [
                        'dist/stuff/libs.css',
                        'dist/stuff/angular-multi-select.css'
                    ]
                }
            }
        },
        clean: {
            script: ['dist/stuff/script.js', 'dist/stuff/script.js.map'],
            libs: ['dist/stuff/libs.js', 'dist/stuff/libs.css', 'dist/stuff/angular-multi-select.js', 'dist/stuff/angular-multi-select.css']
        },
        watch: {
            app: {
                files: ['app/**/*'],
                tasks: ['jade:dest', 'clean:script', 'concat']
            },
            options: {
                nospawn: true,
                interrupt: false,
                debounceDelay: 250,
                livereload: true
            }
        },
        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            script: {
                files: {
                    'dist/stuff/script.js': ['dist/stuff/script.js']
                }
            }
        },
        karma: {
            unit: {
                configFile: 'test/karma.conf.js',
                singleRun: false
            }
        },
        concurrent: {
            server: ['bgShell:runNode', 'watch'],
            options: {
                logConcurrentOutput: true
            }
        }
    });
    grunt.registerTask('dest', ['jade:dest', 'concat', 'bower_concat:all', 'copy:dist', 'uglify:libs', 'cssmin:libs', 'clean:libs']);
    grunt.registerTask('server', ['dest', 'concurrent:server']);
};
