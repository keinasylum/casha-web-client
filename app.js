var express = require('express');
var app = express();
app.set('views', __dirname + '/dist');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.set('staticFolder', './dist/');
app.use(express.static(app.get('staticFolder')));
app.get('/', function(req, res){
    res.render('index');
});
app.get('/components/main/:name', function (req, res) {
    res.render('components/main/' + req.params.name);
});
app.get('/components/operation/add/:name', function (req, res) {
    res.render('components/operation/add/' + req.params.name);
});
app.get('/components/operation/transfer/:name', function (req, res) {
    res.render('components/operation/transfer/' + req.params.name);
});
app.get('/components/operation/edit/:name', function (req, res) {
    res.render('components/operation/edit/' + req.params.name);
});
app.get('/components/statistic/:name', function (req, res) {
    res.render('components/statistic/' + req.params.name);
});
app.get('/components/settings/:name', function (req, res) {
    res.render('components/settings/' + req.params.name);
});
app.get('/components/history/:name', function (req, res) {
    res.render('components/history/' + req.params.name);
});
app.use(function(req, res, next) {
    res.status(404);
    res.render('index');
});
app.use(function(req, res, next) {
    res.status(err.status || 500);
    res.render('index');
});

app.listen(3000, function() {
    console.log("Express server listening on port %d in %s mode", this.address().port, app.settings.env);
});